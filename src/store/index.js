import { createStore } from "vuex";
const employeeData = () => import("@/assets/json/employeeData.json");

export default createStore({
  state: {
    employeeList: [],
    currentEmployeeData: {}
  },
  mutations: {
    SET_EMPLOYEE_LIST(state, payload) {
      state.employeeList = payload;
    },
    SET_CURRENT_EMPLOYEE_DATA(state, payload) {
      state.currentEmployeeData = payload;
    },
    UPDATE_CURRENT_EMPLOYEE_POPULARITY(state, payload) {
      state.employeeList = state.employeeList?.map(item => {
        if (item.name === state.currentEmployeeData.name) {
          item.popularity = payload;
        }
        return item;
      });
    }
  },
  actions: {
    async setEmployeeList({ commit }) {
      let response = await employeeData();
      commit("SET_EMPLOYEE_LIST", response.default.employees);
    },
    async setCurrentEmployeeData({ commit }, payload) {
      commit("SET_CURRENT_EMPLOYEE_DATA", payload);
    },
    async updateCurrentEmployeePopularity({ commit }, payload) {
      commit("UPDATE_CURRENT_EMPLOYEE_POPULARITY", payload);
    }
  },
  modules: {}
});
