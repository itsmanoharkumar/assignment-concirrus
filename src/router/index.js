import { createRouter, createWebHistory } from "vue-router";
import EmployeeDashboard from "../views/EmployeeDashboard.vue";

const routes = [
  {
    path: "/",
    name: "EmployeeDashboard",
    component: EmployeeDashboard
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
